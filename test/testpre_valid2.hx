#if AB
	AB
	#if XY
		AB && XY
	#else
		AB && !XY
	#end
	AB
#else
	!AB
	#if XY
		!AB && XY
	#else
		!AB && !XY
	#end
	!AB
#end