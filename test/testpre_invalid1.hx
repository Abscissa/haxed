// Some Haxe file

import stuff;

#if ERR
	#error
#end

#iffd ENUM
	enum Yum
	{
#if HOT
		Coffee;
		Tea;
#else
		Soda;
		Beer;
#end
	}
#end

class Foo
{
	public function foo()
	{
		var i:Int = #if ONE 1 #elseif TWO 2 #else 3 #end;
	}
}