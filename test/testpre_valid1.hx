// Some Haxe file

import stuff;

#if MORE
	import morestuff;
#end

#if ERRIFNOT
#else
	#error
#end

#if ENUM
	enum Yum
	{
#if HOT
		Coffee;
		Tea;
#else
		Soda;
		Beer;
#end
	}
#end

#if ENUM
enum
#if HOT
enum hot
#else
enum !hot
#end
enum
#else
!enum
#if HOT
!enum hot
#else
!enum !hot
#end
enum
#end

class Foo
{
	public function foo()
	{
		var i:Int = #if ONE 1 #elseif TWO 2 #else 3 #end;
	}
}