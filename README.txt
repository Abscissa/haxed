HaxeD aims to compile Haxe code to D.

HaxeD Homepage:
	http://www.dsource.org/projects/haxed/

Issue tracking is on DSource (*not* on BitBucket):
	View Tickets: http://www.dsource.org/projects/haxed/report
	New Ticket:   http://www.dsource.org/projects/haxed/newticket

Message Board:
	http://www.dsource.org/forums/viewforum.php?f=280

See LICENSE.txt for license information (zlib/libpng license).

-------------------------------------------------

Written in the D programming language: 
    http://www.d-programming-language.org/

This should work with DMD 2.055 and up (excluding 2.057)

-------------------------------------------------

This requires SemiTwist D Tools and Goldie trunk. See HaxeD Homepage
for instructions:
	http://www.dsource.org/projects/haxed/

To compile HaxeD, run this from the main HaxeD directory:
    semitwist-stbuild all

To compile the debug build of HaxeD:
    semitwist-stbuild all debug
	
To re-generate the static language used by this app, run
this from the main HaxeD directory:
    makeStaticLang
