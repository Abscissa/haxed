// HaxeD: Compile Haxe code to D
// Written in the D programming language.

module haxed.util;

import core.thread;
import std.conv;
import std.exception;
import std.stdio;
import std.string;
import std.typecons;

import semitwist.util.all;
import goldie.all;
import lang.hx.all;
import haxed.ast : HxModule;

alias language_hx hx;
alias Token_hx Tok;

enum haxedVerStr = "0.1.1";
enum Ver haxedVer = haxedVerStr.toVer();

template WithToken(T)
{
	alias Tuple!(T, "val", Token, "tok") WithToken;
}

class CompileException : Exception
{
	Data data;
	this(Data data, string msg)
	{
		super(msg);
		this.data = data;
	}
}

class InternalHaxeDException : Exception
{
	Data data;
	Token token;
	this(Data data, Token token, string msg)
	{
		super("Internal Error: "~msg);
		this.data = data;
		this.token = token;
	}
}

enum preprocessExt = ".prep.hx";
enum astExt        = ".ast.json";

struct Options
{
	string dout = "./";
	string[] defines;
	string[] importPaths;
	string[] rdmdArgs;
	bool translateOnly;
	bool ignoreImports;
	bool savePreprocess;
	bool saveAST;
}

struct Data
{
	Options options;
	HxModule[string] modules;
	bool[string] missingImports; // Full names of modules that couldn't be found
	bool hasError = false;
	bool hasNotImplemented = true;

	private alias options opt;
	private bool shownNotImplementedMsg = false;
}
Data data;

void showNotImplementedHelp()
{
	if(!data.shownNotImplementedMsg)
	{
		writeln(`
			--------------------------------------------------------------------
			HaxeD has encountered something in your code that it doesn't yet
			know how to handle. See the "Not Yet Implemented" warning(s) above.

			To ensure this gets fixed, please file a bug report with a
			minimal .hx test case at:
				http://www.dsource.org/projects/goldie/newticket

			If you feel so inclined, contributing a patch to fix the issue
			would be greatly appreciated.
			--------------------------------------------------------------------
		`.normalize());
		
		data.shownNotImplementedMsg = true;
	}
}

void error(Token tok, string msg)
{
	data.hasError = true;
	message(tok, "Error: "~msg);
}

void internalError(Token tok, string msg)
{
	throw new InternalHaxeDException(data, tok, msg);
}

void warning(Token tok, string msg)
{
	message(tok, "Warning: "~msg);
}

void notImplemented(Token tok, string msg)
{
	data.hasNotImplemented = true;
	message(tok, "Not Yet Implemented: "~msg);
}

void message(Token tok, string msg)
{
	if(tok is null)
		writeln(msg);
	else
	{
		auto hiddenExt = preprocessExt;
		auto file = tok.file;
		if(file.endsWith(hiddenExt))
			file = file[0..$-hiddenExt.length] ~ ".hx";
		
		writefln("%s(%s): %s", file, tok.line+1, msg);
	}
}

alias Tok!("<AssignExpr>", "<IterExpr>", "<AssignOp>", "<AssignExpr>") TokAssign;
alias Tok!("<SuffixExpr>", "<SuffixExpr>", ".", "Ident") TokDot;
alias Tok!("<SuffixExpr>", "<SuffixExpr>", "(", "<ExprListOpt>", ")") TokCall;
alias Tok!("<SuffixExpr>", "<SuffixExpr>", "[", "<Expr>", "]") TokIndex;
alias Tok!("<Value>", "(", "<Expr>", ")") TokParenExpr;
alias Tok!("<Value>", "(", "<Stmt>", ")") TokParenStmt;
alias Tok!("<Cast>", "cast", "(", "<Expr>", ",", "<FuncType>", ")") TokCast;
alias Tok!("<Cast>", "cast", "(", "<Expr>", ")") TokUnsafeCast;

alias isAnyType!(
	Tok!"<Expr>",
	Tok!"<AssignExpr>",
	Tok!"<IterExpr>",
	Tok!"<TernaryExpr>",
	Tok!"<LogicOrExpr>",
	Tok!"<LogicAndExpr>",
	Tok!"<CmpBitShiftExpr>",
	Tok!"<AddExpr>",
	Tok!"<MultExpr>",
	Tok!"<PrefixExpr>",
	Tok!"<SuffixExpr>",
	Tok!"<Value>",
	Tok!"<NewExpr>",
	Tok!"<Cast>",
	Tok!"DecLit",
	Tok!"HexLit",
	Tok!"BoolLit",
	Tok!"StrLit",
	Tok!"RegexLit",
	Tok!"<FuncLit>",
	Tok!"<ArrayLit>",
	Tok!"<ObjLit>",
	Tok!"null",
	Tok!"Ident"
) isExprToken;

// Expression tokens that exist only for the sake of order-of-operators
// and don't actually represent a real operation.
alias isAnyType!(
	Tok!("<Expr>",            "<AssignExpr>"     ),
	Tok!("<AssignExpr>",      "<IterExpr>"       ),
	Tok!("<IterExpr>",        "<TernaryExpr>"    ),
	Tok!("<TernaryExpr>",     "<LogicOrExpr>"    ),
	Tok!("<LogicOrExpr>",     "<LogicAndExpr>"   ),
	Tok!("<LogicAndExpr>",    "<CmpBitShiftExpr>"),
	Tok!("<CmpBitShiftExpr>", "<AddExpr>"        ),
	Tok!("<AddExpr>",         "<MultExpr>"       ),
	Tok!("<MultExpr>",        "<PrefixExpr>"     ),
	Tok!("<PrefixExpr>",      "<NewExpr>"        ),
	Tok!("<PrefixExpr>",      "<Cast>"           ),
	Tok!("<PrefixExpr>",      "<SuffixExpr>"     ),
	Tok!("<SuffixExpr>",      "<Value>"          ),
	Tok!("<Value>",           "DecLit"           ),
	Tok!("<Value>",           "HexLit"           ),
	Tok!("<Value>",           "BoolLit"          ),
	Tok!("<Value>",           "StrLit"           ),
	Tok!("<Value>",           "RegexLit"         ),
	Tok!("<Value>",           "<FuncLit>"        ),
	Tok!("<Value>",           "<ArrayLit>"       ),
	Tok!("<Value>",           "<ObjLit>"         ),
	Tok!("<Value>",           "null"             ),
	Tok!("<Value>",           "Ident"            )
) isDummyExprToken;

alias isAnyType!(
	Tok!("<LogicOrExpr>",  "<LogicOrExpr>",  "||", "<LogicAndExpr>"),
	Tok!("<LogicAndExpr>", "<LogicAndExpr>", "&&", "<CmpBitShiftExpr>"),

	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", "==", "<AddExpr>"),
	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", "!=", "<AddExpr>"),
	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", ">=", "<AddExpr>"),
	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", "<=", "<AddExpr>"),
	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", ">",  "<AddExpr>"),
	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", "<",  "<AddExpr>"),

	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", "|", "<AddExpr>"),
	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", "&", "<AddExpr>"),
	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", "^", "<AddExpr>"),

	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", "<<",          "<AddExpr>"),
	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", ">", ">",      "<AddExpr>"),
	Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", ">", ">", ">", "<AddExpr>"),

	Tok!("<AddExpr>", "<AddExpr>", "+", "<MultExpr>"),
	Tok!("<AddExpr>", "<AddExpr>", "-", "<MultExpr>"),

	Tok!("<MultExpr>", "<MultExpr>", "*", "<PrefixExpr>"),
	Tok!("<MultExpr>", "<MultExpr>", "/", "<PrefixExpr>"),
	Tok!("<MultExpr>", "<MultExpr>", "%", "<PrefixExpr>")
) isExprBooleanToken;

class InputVisitor(Elem, Obj) : Fiber
{
	bool started = false;
	Obj obj;
	this(Obj obj)
	{
		this.obj = obj;
		super(&run);
	}

	private void run()
	{
		obj.visit(this);
	}
	
	private void ensureStarted()
	{
		if(!started)
		{
			call();
			started = true;
		}
	}
	
	// Member 'front' must be a function due to DMD Issue #5403
	private Elem _front;
	@property Elem front()
	{
		ensureStarted();
		return _front;
	}
	
	void popFront()
	{
		ensureStarted();
		call();
	}
	
	@property bool empty()
	{
		ensureStarted();
		return state == Fiber.State.TERM;
	}
	
	void yield(Elem elem)
	{
		_front = elem;
		Fiber.yield();
	}
}

InputVisitor!(Elem, Obj) inputVisitor(Elem, Obj)(Obj obj)
{
	return new InputVisitor!(Elem, Obj)(obj);
}

enum MemberOrLocal
{
	member, local
}
string toString(MemberOrLocal location)
{
	return location == MemberOrLocal.member? "member" : "local";
}
