// HaxeD: Compile Haxe code to D
// Written in the D programming language.

/++
The goal of this is to compile Haxe code to D.

$(WEB http://www.dsource.org/projects/haxed/, HaxeD Homepage)

Author:
$(WEB www.semitwist.com, Nick Sabalausky)

This should work with DMD 2.058 and up

This requires SemiTwist D Tools and Goldie master. See HaxeD Homepage link
above for instructions.

To compile HaxeD, run the following command from the main HaxeD directory:
    stbuild all

To compile the debug build of HaxeD:
    stbuild all debug

To re-generate the static language used by this app, run
this from the main HaxeD directory:
    makeStaticLang
+/

//TODO: Think about what happens if a loaded import claims itself to be in a different package.
//TODO: Build resulting .d code with RDMD
//TODO: Goldie Error API enhancements
//TODO: Why doesn't hx.grm work in grmc?
//TODO: --prep-out Set output path for preprocess (implies --save-prep)
//TODO: Profile DMD, why build so slow?
//TODO: Test suite
//TODO: Add a "global" class lookup
//TODO? Goldie: postorder range

module haxed.main;

import getopt = std.getopt;
import std.stdio;

import semitwist.util.all;
import HaxeD = haxed.lib;

int main(string[] args)
{
	bool help;
	HaxeD.Options options;
	
	getopt.endOfOptions = "";
	try getopt.getopt(
		args,
		getopt.config.caseSensitive,
		"help",           &help,
		"cp|import-path", &options.importPaths,
		"D|define",       &options.defines,
		"dout",           &options.dout,
		"ignore-imports", &options.ignoreImports,
		"r|rdmd-arg",     &options.rdmdArgs,
		"save-prep",      &options.savePreprocess,
		"ast",            &options.saveAST,
		"translate-only", &options.translateOnly
	);
	catch(Exception e)
	{
		writeln(e.msg);
		writeln("Run with --help to see usage.");
		return 0;
	}
	
	if(help || args.length < 2 || args.contains("/?"))
	{
		write(HaxeD.helpScreen);
		return 0;
	}

	auto files = args[1..$];
	
	try HaxeD.compile(files, options);
	catch(HaxeD.CompileException e)
	{
		if(e.msg !is null)
			writeln(e.msg);
		
		return 1;
	}
	
	return 0;
}
