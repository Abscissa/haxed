// HaxeD: Compile Haxe code to D
// Written in the D programming language.

module haxed.ast;

import std.conv;
import std.file;
import std.path;
import std.range;
import std.stdio;
import std.string;
import std.typetuple;

import semitwist.treeout;
import semitwist.util.all;
import goldie.all;
import lang.hx.all;
import haxepred.lib;
import haxed.util;

void loadImport(string importName, Token srcTok)
{
	if(data.opt.ignoreImports || importName in data.modules)
		return;
	
	if(importName in data.missingImports)
	{
		error(srcTok, "Couldn't find module '"~importName~"'");
		return;
	}

	auto relFilePath = importName.replace(".", "/") ~ ".hx";
	
	foreach(basePath; data.opt.importPaths)
	if(exists(basePath~relFilePath))
	{
		// Add a placeholder to prevent cycles
		data.modules[importName] = null;
		
		loadHx(basePath~relFilePath);
		return;
	}
	
	error(srcTok, "Couldn't find module '"~importName~"'");
	data.missingImports[importName] = true;
}

void loadHx(string filepath)
{
	Tok!"<Module>" parseTree;
	HxModule mod;
	bool loadedOk = true;
	try
	{
		auto source = readUTFFile!string(filepath);
		auto preprocessedCode = HaxePreD.preprocessString(source, data.opt.defines);
		if(data.opt.savePreprocess)
		{
			auto prepath = filepath.setExtension(preprocessExt);
			std.file.write(prepath, preprocessedCode);
		}

		parseTree = hx.parseCode(preprocessedCode, filepath).parseTree;
		mod = new HxModule(parseTree, filepath, source);
	}
	catch(ParseException e)
	{
		writeln(e.msg);
		loadedOk = false;
		data.hasError = true;
	}
	catch(PreprocessException e)
	{
		writeln(filepath.replace("\\", "/"), e.msg);
		loadedOk = false;
		data.hasError = true;
	}

	if(!loadedOk)
		return;

	data.modules[mod.fullName] = mod;
	
	writeln("Module: ", mod.fullName);
	foreach(imp; mod.imports)
		writeln("  I: ", imp.val);
}

struct NamedNode
{
	string name;
	HxNode node;
}
template toNamedNode(alias var)
{
	enum toNamedNode = `NamedNode("`~var.stringof~`", `~var.stringof~`)`;
}
alias InputVisitor!(NamedNode, HxNode) HxNodeVisitor;
alias inputVisitor!(NamedNode, HxNode) hxNodeVisitor;

// Derived types must come BEFORE their base types
alias TypeTuple!(
	HxMultiStatement,
	HxStmtNotImplemented,
	HxStmtExpression,
	HxStmtReturn,
	HxStmtIf,
	HxStmtSwitch,
	HxStmtBreak,
	HxStmtContinue,
	HxStmtCase,
	HxStmtDefault,
	HxStmtWhile,

	HxExprNotImplemented,
	HxExprStatement,
	HxExprAssign,
	HxExprDot,
	HxExprCall,
	HxExprIndex,
	HxExprParen,
	HxValueIdent,
	HxValueDecLit,
	HxValueStrLit,
	HxExprUnaryPrefix,
	HxExprUnarySuffix,
	HxExprBinary,
	HxExprNew,
	HxExprCast,
	
	HxModule,
	HxEnum,
	HxClass,
	HxDeclAttr,
	HxVarDecl,
	HxType,
	HxFunc,
	HxStatement,
	HxExpression,

	HxNode,
) AllNodeTypes;

Tok!() getToken(HxNode anyNode)
{
	foreach(T; AllNodeTypes)
	if(auto node = cast(T)anyNode)
	{
		static if(
			is(T == HxDeclAttr)  || is(T == HxNode) ||
			is(T == HxStatement) || is(T == HxExpression)
		)
		{
			return null;
		}

		else static if( is(T == HxStmtExpression) )
			return getToken(node.expr);

		else
			return node.token;
	}
	
	internalError(null, "Unhandled node type: "~anyNode.classinfo.name);
	assert(0);
}

abstract class HxNode
{
	abstract void visit(HxNodeVisitor v);

	final TreeNode toTreeNode(size_t index=0, string name="")
	{
		immutable thisModuleName = "haxed.ast.";
		auto className = this.classinfo.name;
		assert(className.startsWith(thisModuleName), "Did HaxeD's module names change?");
		className = className[thisModuleName.length..$];
		
		if(name != "")
			name ~= ": ";
		auto node = new TreeNode(name~className);
		auto tok = getToken(this);

		if(tok)
		{
			auto start = tok.srcIndexStart;
			node.addAttribute("srcIndexStart", start);
			node.addAttribute("srcLength", tok.srcIndexEnd - start);
			node.addAttribute("line", tok.line+1);
		}
		
		int i=0;
		foreach(elem; hxNodeVisitor(this))
		{
			if(elem.node)
				node.addContent(elem.node.toTreeNode(i, elem.name));
			else
				node.addAttribute(elem.name, "{null}");
			i++;
		}

		customizeTreeNode(node);
		
		return node;
	}

	void customizeTreeNode(TreeNode node)
	{
		// Do nothing
	}

	abstract string toD();
}

class HxModule : HxNode
{
	Tok!"<Module>" token;
	string hxFile;
	string dFile;
	string pack; // package
	string name;
	string fullName;
	string source;
	WithToken!string[] imports;
	HxNode[]           members;
	HxClass[string]    classes;
	HxEnum[string]     enums;
	
	this(Tok!"<Module>" token, string hxFile, string source)
	{
		this.token = token;
		this.hxFile = hxFile;
		this.source = source;
		
		// Check for any remaining preprocessor directives
		foreach(t; traverse(token))
		if(auto tPP = cast(Tok!"<PP>")t)
			internalError(t, "Preprocessor missed a directive.");
		
		// Package
		foreach(t; traverse!( isAnyType!(Tok!"<Package>", Tok!"<Module>") )(token))
		if(auto tokPack = cast(Tok!"<Package>")t)
			pack = tokPack.getRequired!(Tok!"<DotIdent>")().toStringCompact();
		
		// Misc Metadata
		name = hxFile.baseName().stripExtension();
		fullName = (pack is null)? name : pack ~ "." ~ name;
		dFile = data.opt.dout ~ fullName.replace(".", "/") ~ ".d";
		
		auto tokTLL = cast(Tok!"<TopLevelList>")token.getRequired!(Tok!"<TopLevelList>")();

		// Imports
		imports = findImportDecls(tokTLL);
		foreach(importName; retro(imports))
			loadImport(importName.val, importName.tok);
		
		// Declarations
		alias isAnyType!(
			Tok!"<TopLevelList>", Tok!"<TopLevel>", Tok!"<TopLevelDecl>",
			Tok!"<ClassDecl>", Tok!"<EnumDecl>"
		) pred;
		foreach(t; traverse!pred(tokTLL))
		{
			if(auto tokClass = cast(Tok!"<ClassDecl>")t)
			{
				auto newClass = new HxClass(tokClass, this);
				members ~= newClass;
				classes[newClass.name] = newClass;
			}

			else if(auto tokEnum = cast(Tok!"<EnumDecl>")t)
			{
				auto newEnum = new HxEnum(tokEnum, this);
				members ~= newEnum;
				enums[newEnum.name] = newEnum;
			}
		}
	}

	private static WithToken!string[] findImportDecls(Tok!"<TopLevelList>" list)
	{
		WithToken!string[] found;

		foreach(t; traverse!( isAnyType!(Tok!"<TopLevelList>", Tok!"<TopLevel>", Tok!"<Import>") )(list))
		if(auto tokImport = cast(Tok!"<Import>")t)
			found ~= WithToken!string(tokImport[1].toStringCompact(), tokImport);

		return found;
	}

	override void visit(HxNodeVisitor v)
	{
		foreach(i, elem; enums)
			v.yield(NamedNode("enums[%s]".format(i), elem));

		foreach(i, elem; classes)
			v.yield(NamedNode("classes[%s]".format(i), elem));
	}

	void saveAST()
	{
		auto root = toTreeNode();
		auto json = root.format(formatterTrimmedJSON);

		auto path = hxFile.setExtension(astExt);
		std.file.write(path, json);
	}
	
	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("file", hxFile);
		node.addAttribute("parseTreeMode", "true");
		node.addAttribute("source", source);

		node.addAttribute("hxFile", hxFile);
		node.addAttribute("dFile", dFile);
		node.addAttribute("package", pack);
		node.addAttribute("name", name);
		node.addAttribute("fullName", fullName);
		
		foreach(i, elem; imports)
		{
			auto importNode = new TreeNode("imports[%s]: %s".format(i, elem.val));
			importNode.addAttribute("name", elem.val);
			auto tok = elem.tok;

			if(tok)
			{
				auto start = tok.srcIndexStart;
				importNode.addAttribute("srcIndexStart", start);
				importNode.addAttribute("srcLength", tok.srcIndexEnd - start);
				importNode.addAttribute("line", tok.line+1);
			}
			
			node.addContent(importNode);
		}
	}

	override string toD()
	{
		string d;
		
		d ~= (`
			// Generated by HaxeD v`~haxedVerStr~` (http://www.dsource.org/projects/haxed/)
			// Written in the D programming language.

			module `~fullName~`;
		`).normalize() ~ "\n";

		if(imports.length > 0)
		{
			d ~= "\n";
			foreach(importModule; imports)
				d ~= "import "~importModule.val~";\n";
		}
		
		if(members.length > 0)
		foreach(m; members)
		{
			d ~= "\n";
			d ~= m.toD();
		}
		
		return d;
	}
	
	void saveD()
	{
		auto dir = dFile.dirName();
		if(!exists(dir))
			mkdirRecurse(dir);

		std.file.write(dFile, toD());
	}
}

class HxEnum : HxNode
{
	Tok!"<EnumDecl>" token;
	string name;
	string fullName;
	HxModule mod; // module

	this(Tok!"<EnumDecl>" token, HxModule mod)
	{
		this.token = token;
		this.mod = mod;
		
		name = token.getRequired!(Tok!"Ident")().toString();
		fullName = name ~ "." ~ mod.fullName;
	}

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("name", name);
		node.addAttribute("fullName", fullName);
	}

	override string toD()
	{
		return (`
			enum `~name~`
			{
				
			}
		`).normalize() ~ "\n";
	}
}

class HxClass : HxNode
{
	Tok!"<ClassDecl>" token;
	string name;
	string fullName;
	HxModule mod; // module
	HxNode[] members;
	HxVarDecl[string] vars;
	HxFunc   [string] funcs;

	this(Tok!"<ClassDecl>" token, HxModule mod)
	{
		this.token = token;
		this.mod = mod;
		
		name = token.getRequired!(Tok!"Ident")().toString();
		fullName = name ~ "." ~ mod.fullName;
		
		// Members
		auto r = traverse(token.getRequired!(Tok!"<ClassBody>")());
		foreach(t; r)
		{
			if(auto tokDecl = cast(Tok!"<VarDecl>")t)
			{
				foreach(v; HxVarDecl.findAll(tokDecl, mod, MemberOrLocal.member))
				{
					members ~= v;
					vars[v.name] = v;
				}
					
				r.skip();
			}
			else if(auto tokDecl = cast(Tok!"<FuncDecl>")t)
			{
				auto func = new HxFunc(tokDecl, this);
				members ~= func;
				funcs[func.name] = func;
					
				r.skip();
			}
		}
	}

	override void visit(HxNodeVisitor v)
	{
		foreach(i, elem; vars)
			v.yield(NamedNode("vars[%s]".format(i), elem));

		foreach(i, elem; funcs)
			v.yield(NamedNode("funcs[%s]".format(i), elem));
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("name", name);
		node.addAttribute("fullName", fullName);
	}

	override string toD()
	{
		string membersStr;
		foreach(v; members)
			membersStr ~= v.toD();
			
		return (`
			class `~name~`
			{
			!MEMBERS!
			}
		`).normalize().replace("!MEMBERS!", membersStr.indent().stripRight()) ~ "\n";
	}
}

// Declaration Attributes
class HxDeclAttr : HxNode
{
	bool isPublic   = false;
	bool isStatic   = false;
	bool isInline   = false;
	bool isDynamic  = false;
	bool isOverride = false;
	MemberOrLocal location;
	
	this(Tok!"<DeclAttrList>" token, MemberOrLocal location)
	{
		this.location = location;
		
		if(token is null)
			return;
		
		isPublic = location == MemberOrLocal.local;
		
		bool hasFoundAccess = false;
		void foundAccess(Token t)
		{
			if(hasFoundAccess)
				error(t, "Must use either 'public' or 'private' only once.");
			
			hasFoundAccess = true;
		}
		
		foreach(t; traverse(token))
		{
			if(cast(Tok!"public")t)
			{
				foundAccess(t);
				isPublic = true;
			}
			else if(cast(Tok!"private")t)
			{
				foundAccess(t);
				isPublic = false;
			}
			else if(cast(Tok!"static")t)
				isStatic = true;
			else if(cast(Tok!"inline")t)
				isInline = true;
			else if(cast(Tok!"dynamic")t)
			{
				error(t, "Not supported: 'dynamic'");
				isDynamic = true;
			}
			else if(cast(Tok!"override")t)
			{
				if(location == MemberOrLocal.local)
					error(t, "Only member functions can be override");

				isOverride = true;
			}
		}
	}

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("isPublic", isPublic);
		node.addAttribute("isStatic", isStatic);
		node.addAttribute("isInline", isInline);
		node.addAttribute("isDynamic", isDynamic);
		node.addAttribute("isOverride", isOverride);
		node.addAttribute("location", .toString(location));
	}

	override string toD()
	{
		string[] strings;
		
		if(location == MemberOrLocal.member)
			strings ~= isPublic? "public" : "protected";
		
		if(isStatic)
			strings ~= "static";
		
		if(isOverride)
			strings ~= "override";
		
		if(isInline)
			strings ~= "immutable";
		
		return std.string.join(strings, " ");
	}
}

// Variable Declaration
class HxVarDecl : HxNode
{
	Tok!() token;
	string name;
	string fullName;
	bool isParam;
	HxModule   mod; // module
	HxDeclAttr attr;
	HxType     type;
	
	private this(Tok!() token, HxModule mod, HxDeclAttr attr, bool isParam)
	{
		this.token   = token;
		this.mod     = mod;
		this.attr    = attr;
		this.isParam = isParam;
		
		name = token.getRequired!(Tok!"Ident")().toString();
		fullName = name ~ "." ~ mod.fullName;

		type = new HxType(
			token.get!(Tok!"<TypeTagOpt>", Tok!"<TypeTag>", Tok!"<FuncType>")()
		);
	}
	
	this(Tok!"<VarDeclPart>" token, HxModule mod, HxDeclAttr attr)
	{
		this(cast(Tok!())token, mod, attr, false);
	}
	
	this(Tok!"<Param>" token, HxModule mod)
	{
		this(cast(Tok!())token, mod, new HxDeclAttr(null, MemberOrLocal.local), true);
	}
	
	private static HxVarDecl[] findAll(Tok!"<VarDecl>" tokVarDecl, HxModule mod, MemberOrLocal location)
	{
		HxVarDecl[] found;
		auto attr = new HxDeclAttr(tokVarDecl.getRequired!(Tok!"<DeclAttrList>"), location);

		foreach(t; traverse(tokVarDecl.getRequired!(Tok!"<VarDeclPartList>")))
		if(auto tokPart = cast(Tok!"<VarDeclPart>")t)
			found ~= new HxVarDecl(tokPart, mod, attr);

		return found;
	}

	private static HxVarDecl[] findAll(Tok!"<ParamList>" tokParamList, HxClass cl)
	{
		if(!tokParamList)
			return null;

		HxVarDecl[] found;
		
		auto range = traverse(tokParamList);
		foreach(t; range)
		if(auto tokParam = cast(Tok!"<Param>")t)
		{
			found ~= new HxVarDecl(tokParam, cl.mod);
			range.skip();
		}

		return found;
	}

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!attr));
		v.yield(mixin(toNamedNode!type));
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("name", name);
		node.addAttribute("fullName", fullName);
		node.addAttribute("isParam", isParam);
	}

	override string toD()
	{
		auto attrStr = attr.toD();
		if(attrStr != "")
			attrStr ~= " ";
		
		auto str = attrStr~type.toD()~" "~name;

		if(!isParam)
			str ~= ";\n";

		return str;
	}
}

class HxType : HxNode
{
	Tok!() token;
	string name;
	bool isAuto = false;
	
	this(Tok!"<FuncType>" token)
	{
		this.token = token;
		
		if(token is null)
		{
			isAuto = true;
			return;
		}
		
		if(auto tokFuncType = cast(Tok!("<FuncType>", "<FuncType>", "->", "<Type>"))token)
			error(token, "Delegates not yet supported");
		else if(auto tokFuncType = cast(Tok!("<FuncType>", "<Type>"))token)
			ctorHelper( tokFuncType.sub!0 );
		else
			internalError(token, "Unexpected <FuncType> rule");
	}
	
	this(Tok!"<Type>" token)
	{
		this.token = token;
		
		if(token is null)
		{
			isAuto = true;
			return;
		}
		
		ctorHelper(token);
	}
	
	private void ctorHelper(Tok!"<Type>" token)
	{
		if(auto tokType = cast(Tok!("<Type>", "<Type>", "<TypeParam>"))token)
			error(token, "Generics not yet supported");
		else if(auto tokType = cast(Tok!("<Type>", "<AnonType>"))token)
			error(token, "Anonymous types not yet supported");
		else if(auto tokType = cast(Tok!("<Type>", "<DotIdent>"))token)
			name = tokType[0].toStringCompact();
		else
			internalError(token, "Unexpected <Type> rule");
	}

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("name", name);
		node.addAttribute("isAuto", isAuto);
	}

	override string toD()
	{
		if(isAuto)
			return "auto";
		
		switch(name)	
		{
		case "String": return "string";
		case "Bool":   return "bool";
		case "Int":    return "int";
		case "Float":  return "double";
		case "Void":   return "void";
		default:       return name;
		}
	}
}

// Function
class HxFunc : HxNode
{
	Tok!"<FuncDecl>" token;
	string name;
	HxClass cl; // class
	HxDeclAttr attr;
	HxType returnType;
	HxVarDecl[] params;
	HxVarDecl[string] paramLookup;
	HxMultiStatement body_;

	this(Tok!"<FuncDecl>" token, HxClass cl)
	{
		this.token = token;
		this.cl = cl;
		
		if(auto tokName = token.get("Ident"))
			name = tokName.toString();
		else
			name = "this";
		
		// Attributes
		attr = new HxDeclAttr(
			token.get!(Tok!"<DeclAttrList>")(),
			MemberOrLocal.member
		);

		// Return Type
		returnType =
			new HxType(
				token.get!(Tok!"<TypeTagOpt>", Tok!"<TypeTag>", Tok!"<FuncType>")()
			);
		
		// Params
		params = HxVarDecl.findAll(
			token.getRequired!( Tok!"<ParamListOpt>", Tok!"<ParamList>" )(),
			cl
		);
		foreach(p; params)
			paramLookup[p.name] = p;
		
		// Body
		body_ = new HxMultiStatement( token.getRequired!(Tok!"<BlockStmt>")() );
	}

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!attr));
		v.yield(mixin(toNamedNode!returnType));
		
		foreach(i, elem; params)
			v.yield(NamedNode("params[%s]".format(i), elem));
		
		v.yield(mixin(toNamedNode!body_));
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("name", name);
	}

	override string toD()
	{
		auto retTypeStr = name=="this"? "" : (returnType.toD()~" ");
		
		string paramStr;
		foreach(p; params)
		{
			if(paramStr != "")
				paramStr ~= ", ";
				
			paramStr ~= p.toD();
		}
		
		auto headerStr = attr.toD() ~ " " ~ retTypeStr ~ name ~ "("~paramStr~")";
		return `
			!HEAD!
			{
			!BODY!
			}
		`.normalize()
		.replace("!HEAD!", headerStr)
		.replace("!BODY!", body_.toD().indent())
		~"\n";
	}
}

// -- Statements ---------------------------------------
abstract class HxStatement : HxNode
{
	static HxStatement fromToken(Token token)
	{
		if(auto tokStmt = cast(Tok!"<Stmt>")token)
			return fromToken(tokStmt);
		else if(auto tokStmt = cast(Tok!"<ThenStmt>")token)
			return fromToken(tokStmt);
		else if(auto tokStmt = cast(Tok!"<ThenOkStmt>")token)
			return fromToken(tokStmt);
		else if(auto tokStmt = cast(Tok!"<ThenNotOkStmt>")token)
			return fromToken(tokStmt);
		
		internalError(token, "Unexpected <Stmt> rule");
		assert(0);
	}

	static HxStatement fromToken(Tok!"<Stmt>" token)
	{
		if(auto tokStmt = token.get!(Tok!"<ThenOkStmt>")())
			return fromToken(tokStmt);
		else if(auto tokStmt = token.get!(Tok!"<ThenNotOkStmt>")())
			return fromToken(tokStmt);

		internalError(token, "Unexpected <Stmt> rule");
		assert(0);
	}

	static HxStatement fromToken(Tok!"<ThenOkStmt>" token)
	{
		if(auto tokStmt = token.get!(Tok!"<Expr>")())
		{
			auto expr = HxExpression.fromToken(tokStmt, null);
			auto stmt = new HxStmtExpression(expr);
			expr.parent = stmt;
			return stmt;
		}
		else if(auto tokStmt = token.get!(Tok!"<BlockStmt>")())
			return new HxMultiStatement(tokStmt);
		else if(auto tokStmt = token.get!(Tok!"<ReturnStmt>")())
			return new HxStmtReturn(tokStmt);
		else if(auto tokStmt = token.get!(Tok!"<SwitchStmt>")())
			return new HxStmtSwitch(tokStmt);
		else if(auto tokStmt = token.get!(Tok!"<BreakStmt>")())
			return new HxStmtBreak(tokStmt);
		else if(auto tokStmt = token.get!(Tok!"<ContinueStmt>")())
			return new HxStmtContinue(tokStmt);
		else if(auto tokStmt = token.get!(Tok!"<CaseStmt>")())
			return new HxStmtCase(tokStmt);
		else if(auto tokStmt = token.get!(Tok!"<DefaultStmt>")())
			return new HxStmtDefault(tokStmt);
		else
		{
			notImplemented(token[0][0], "Statement of type '"~token[0][0].name~"'");
			return new HxStmtNotImplemented(token);
		}
	}

	static HxStatement fromToken(Tok!"<ThenNotOkStmt>" token)
	{
		if(auto tokStmt = token.get!(Tok!"<IfStmt>")())
			return new HxStmtIf(tokStmt);
		if(auto tokStmt = token.get!(Tok!"<WhileStmt>")())
			return new HxStmtWhile(tokStmt);
		else
		{
			notImplemented(token[0][0], "Statement of type '"~token[0][0].name~"'");
			return new HxStmtNotImplemented(token);
		}
	}
}

class HxMultiStatement : HxStatement
{
	Tok!() token;
	HxStatement[] statements;
	
	this(Tok!() token)
	{
		this.token = token;
	}

	this(Tok!"<BlockStmt>" token)
	{
		this.token = token;

		foreach(t; traverse!( isAnyType!(Tok!"<StmtList>", Tok!"<Stmt>") )(token))
		if(auto tokAnyStmt = cast(Tok!"<Stmt>")t)
			statements ~= HxStatement.fromToken(tokAnyStmt);
	}

	override void visit(HxNodeVisitor v)
	{
		foreach(i, elem; statements)
			v.yield(NamedNode("statements[%s]".format(i), elem));
	}

	override string toD()
	{
		string str;
		foreach(stmt; statements)
		{
			if(str != "")
				str ~= "\n";
			
			str ~= stmt.toD();
		}
		return str;
	}
}

class HxStmtNotImplemented : HxStatement
{
	Tok!() token;
	string symbolName;

	this(Tok!"<Stmt>" token)
	{
		this.token = token;
		symbolName = token[0][0].name;
	}

	this(Tok!"<ThenOkStmt>" token)
	{
		this.token = token;
		symbolName = token[0].name;
	}

	this(Tok!"<ThenNotOkStmt>" token)
	{
		this.token = token;
		symbolName = token[0].name;
	}

	this(Tok!"<ThenStmt>" token)
	{
		this.token = token;
		symbolName = token.name;
	}

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("symbolName", symbolName);
	}

	override string toD()
	{
		auto src =
			q{static assert(false, "HaxeD: This 'SYM_NAME' statement not yet implemented");}
			.replace("SYM_NAME", symbolName);

		return src;
	}
}

// An expression used as a statement
class HxStmtExpression : HxStatement
{
	HxExpression expr;
	
	this(HxExpression expr)
	{
		this.expr = expr;
	}

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!expr));
	}

	override string toD()
	{
		return expr.toD() ~ ";";
	}
}

class HxStmtReturn : HxStatement
{
	Tok!"<ReturnStmt>" token;
	HxExpression expr;

	this(Tok!"<ReturnStmt>" token)
	{
		this.token = token;
		auto tokExpr = token.get!(Tok!"<Expr>")();
		if(tokExpr)
			expr = HxExpression.fromToken(tokExpr, this);
	}

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!expr));
	}

	override string toD()
	{
		string exprStr;
		if(expr)
		{
			exprStr = " ";
			exprStr ~= expr.toD();
		}

		return "return"~exprStr~";";
	}
}

class HxStmtIf : HxStatement
{
	Tok!() token;
	HxExpression cond;
	HxStatement body_;
	HxStatement else_;

	this(Tok!() token)
	{
		if( !isAnyType!(Tok!"<IfStmt>", Tok!"<ThenStmt>")(token) )
			internalError(token, "Token is neither <IfStmt> nor <ThenStmt>");
		
		this.token = token;
		cond = HxExpression.fromToken( token.getRequired!(Tok!"<Expr>")(), this );
		body_ = HxStatement.fromToken( token[4] );
		if(token.length == 7)
			else_ = HxStatement.fromToken( token[6] );
	}

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!cond ));
		v.yield(mixin(toNamedNode!body_));
		v.yield(mixin(toNamedNode!else_));
	}

	override string toD()
	{
		if(else_)
		{
			if(cast(HxStmtIf)else_)
			{
				return `
					if(!COND!)
					{
					!BODY!
					}
					else !ELSE!
				`.normalize()
				.replace("!COND!", cond.toD())
				.replace("!BODY!", body_.toD().indent())
				.replace("!ELSE!", else_.toD());
			}
			else
			{
				return `
					if(!COND!)
					{
					!BODY!
					}
					else
					{
					!ELSE!
					}
				`.normalize()
				.replace("!COND!", cond.toD())
				.replace("!BODY!", body_.toD().indent())
				.replace("!ELSE!", else_.toD().indent());
			}
		}
		else
		{
			return `
				if(!COND!)
				{
				!BODY!
				}
			`.normalize()
			.replace("!COND!", cond.toD())
			.replace("!BODY!", body_.toD().indent());
		}
	}
}

class HxStmtSwitch : HxStatement
{
	Tok!"<SwitchStmt>" token;
	HxExpression expr;
	HxMultiStatement body_;

	this(Tok!"<SwitchStmt>" token)
	{
		this.token = token;
		expr = HxExpression.fromToken( token.getRequired!(Tok!"<Expr>")(), this );
		body_ = new HxMultiStatement( token.getRequired!(Tok!"<BlockStmt>")() );
		
		//TODO: What does Haxe do upon 'switch(foo){case bar: break;}' and
		//      'for(...) { switch(foo){case bar: break;} }'?
		//
		//      *MAYBE*:
		//      For any 'break' inside 'body_' (deep inside not shallow), if
		//      it isn't inside another 'switch'/'do'/'while'/'for'/'foreach',
		//      it should be adjusted to break to the proper location.

		//TODO? Do I need to move all 'default' groups to the end?
		
		// Insert 'break' statements at end of each 'case'/'default'
		HxStatement[] newStatements;
		bool foundCase=false; // Has a 'case' or 'default' been found?
		foreach(i, oldStmt; body_.statements)
		{
			if(cast(HxStmtCase)oldStmt || cast(HxStmtDefault)oldStmt)
			{
				if(foundCase)
				{
					//TODO: Don't insert a 'break' if the previous statement
					//      was 'throw' or 'return'.
					newStatements ~= new HxStmtBreak(null);
				}
				foundCase = true;
			}
			newStatements ~= oldStmt;
			
			if(foundCase && i == body_.statements.length-1)
				//TODO: Don't insert a 'break' if the previous statement
				//      was 'throw' or 'return'.
				newStatements ~= new HxStmtBreak(null);
				
		}
		body_.statements = newStatements;
	}

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!expr));
		v.yield(mixin(toNamedNode!body_));
	}

	override string toD()
	{
		return `
			switch(!EXPR!)
			{
			!BODY!
			}
		`.normalize()
		.replace("!EXPR!", expr.toD())
		.replace("!BODY!", body_.toD().indent())
		~"\n";
	}
}

class HxStmtBreak : HxStatement
{
	Tok!"<BreakStmt>" token;

	this(Tok!"<BreakStmt>" token)
	{
		this.token = token;
	}

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override string toD()
	{
		return "break;";
	}
}

class HxStmtContinue : HxStatement
{
	Tok!"<ContinueStmt>" token;

	this(Tok!"<ContinueStmt>" token)
	{
		this.token = token;
	}

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override string toD()
	{
		return "continue;";
	}
}

class HxStmtCase : HxStatement
{
	Tok!"<CaseStmt>" token;
	HxExpression[] expressions;
	
	this(Tok!"<CaseStmt>" token)
	{
		this.token = token;
		expressions = HxExpression.findAll( token.getRequired!(Tok!"<ExprList>")(), this );
	}

	override void visit(HxNodeVisitor v)
	{
		foreach(i, elem; expressions)
			v.yield(NamedNode("expressions[%s]".format(i), elem));
	}

	override string toD()
	{
		string str;
		foreach(expr; expressions)
		{
			if(str.length == 0)
				str ~= "case ";
			else
				str ~= ", ";

			str ~= expr.toD();
		}
		return str~":";
	}
}

class HxStmtDefault : HxStatement
{
	Tok!"<DefaultStmt>" token;

	this(Tok!"<DefaultStmt>" token)
	{
		this.token = token;
	}

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override string toD()
	{
		return "default:";
	}
}

class HxStmtWhile : HxStatement
{
	Tok!"<WhileStmt>" token;
	bool isDoWhile;
	HxExpression expr;
	HxStatement stmt;

	this(Tok!"<WhileStmt>" token)
	{
		this.token = token;
		expr = HxExpression.fromToken( token.getRequired!(Tok!"<Expr>"), this );
		stmt = HxStatement .fromToken( token.getRequired!(Tok!"<Stmt>") );

		auto firstSymbolName = token[0].symbol.name;
		assert(firstSymbolName == "do" || firstSymbolName == "while");
		isDoWhile = firstSymbolName == "do";
	}

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!expr));
		v.yield(mixin(toNamedNode!stmt));
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("isDoWhile", isDoWhile);
	}

	override string toD()
	{
		string templ;
		if(isDoWhile)
		{
			templ = `
				do
				{
				!BODY!
				} while(!EXPR!);
			`.normalize();
		}
		else
		{
			templ = `
				while(!EXPR!)
				{
				!BODY!
				}
			`.normalize();
		}
		
		return
			templ
			.replace("!EXPR!", expr.toD())
			.replace("!BODY!", stmt.toD().indent());
	}
}

// -- Expressions ---------------------------------------
string toD(HxExpression[] expressions)
{
	string str;
	foreach(expr; expressions)
	{
		if(str.length != 0)
			str ~= ", ";
			
		str ~= expr.toD();
	}
	return str;
}

abstract class HxExpression : HxNode
{
	static HxExpression[] findAll(Tok!"<ExprListOpt>" token, HxNode parent)
	{
		if( cast(Tok!("<ExprListOpt>", null))token )
			return null;

		return findAll( token.getRequired!(Tok!"<ExprList>"), parent );
	}

	static HxExpression[] findAll(Tok!"<ExprList>" token, HxNode parent)
	{
		HxExpression[] found;

		foreach(currTok; traverse!( isAnyType!(Tok!"<ExprList>", Tok!"<Expr>") )(token))
		if(auto tokExpr = cast(Tok!"<Expr>")currTok)
			found ~= HxExpression.fromToken(tokExpr, parent);
		
		return found;
	}

	static HxExpression fromToken(Token token, HxNode parent)
	{
		while(true)
		{
			if(!isExprToken(token))
				internalError(token, "Symbol '"~token.name~"' isn't an expression");

			// Dive down to first parse-tree node with a real expression
			if(isDummyExprToken(token))
			{
				token = token[0];
				continue;
			}
			
			// Create AST node for expression
			if(auto tokExpr = cast(TokAssign)token)
				return new HxExprAssign(tokExpr, parent);
			else if(auto tokExpr = cast(TokDot)token)
				return new HxExprDot(tokExpr, parent);
			else if(auto tokExpr = cast(TokCall)token)
				return new HxExprCall(tokExpr, parent);
			else if(auto tokExpr = cast(TokIndex)token)
				return new HxExprIndex(tokExpr, parent);
			else if(auto tokExpr = cast(TokParenExpr)token)
				return new HxExprParen(tokExpr, parent);
			else if(auto tokExpr = cast(TokParenStmt)token)
				return new HxExprParen(tokExpr, parent);
			else if(auto tokExpr = cast(Tok!"Ident")token)
				return new HxValueIdent(tokExpr, parent);
			else if(auto tokExpr = cast(Tok!"DecLit")token)
				return new HxValueDecLit(tokExpr, parent);
			else if(auto tokExpr = cast(Tok!"StrLit")token)
				return new HxValueStrLit(tokExpr, parent);
			else if(auto tokExpr = cast(Tok!"<NewExpr>")token)
				return new HxExprNew(tokExpr, parent);
			else if(auto tokExpr = cast(TokCast)token)
				return new HxExprCast(tokExpr, parent);
			else if(auto tokExpr = cast(Tok!"<PrefixExpr>")token)
				return new HxExprUnaryPrefix(tokExpr, parent);
			else if(auto tokExpr = cast(Tok!"<SuffixExpr>")token)
				return new HxExprUnarySuffix(tokExpr, parent);
			else if(isExprBooleanToken(token))
				return new HxExprBinary(cast(Tok!())token, parent);
			else
			{
				notImplemented(token, "Expression of type '"~token.name~"'");
				return new HxExprNotImplemented(cast(Tok!())token, parent);
			}
		}
	}
	
	HxNode parent;
	HxExpression lhs;
	HxExpression rhs;
	
	this(HxNode parent, HxExpression lhs, HxExpression rhs)
	{
		this.parent = parent;
		this.lhs    = lhs;
		this.rhs    = rhs;
	}
	
	/// Operator precedence priority for D.
	/// Higher numbers are higher priority (ie. binds tighter).
	abstract @property int dPriority();

	@property int isAmbiguousDPriority(HxExpression target)
	{
		return false;
	}
	
	@property bool isDLeftAssoc()
	{
		return true;
	}
	
	final @property bool needSurroundingParens()
	{
		auto parentExpr = cast(HxExpression)parent;
		
		// Is priority applicable?
		if(!parentExpr)
			return false;
		
		if(parentExpr.lhs !is this && parentExpr.rhs !is this)
			return false;
		
		// Is priority ambiguous for D?
		if(isAmbiguousDPriority(parentExpr))
			return true;
		
		// Is priority wrong for D?
		if(dPriority < parentExpr.dPriority)
			return true;
		
		if(dPriority > parentExpr.dPriority)
			return false;
		
		// Equal priority: Is associativity wrong for D?
		if(parentExpr.lhs is this && parentExpr.isDLeftAssoc)
			return false;

		if(parentExpr.rhs is this && !parentExpr.isDLeftAssoc)
			return false;
		
		return true;
	}

	final override string toD()
	{
		return needSurroundingParens? "("~toDExpr~")" : toDExpr;
	}
	
	abstract string toDExpr();
}

class HxExprNotImplemented : HxExpression
{
	Tok!() token;

	this(Tok!() token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
	}

	override @property int dPriority() { return -1; }

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override string toDExpr()
	{
		return q{
			(){static assert(false, "HaxeD: This 'SYM_NAME' expression not yet implemented");}
		}.strip().replace("SYM_NAME", token.name);
	}
}

// A statement used as an expression
class HxExprStatement : HxExpression
{
	TokParenStmt token;
	HxStatement stmt;

	this(TokParenStmt token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
		stmt = HxStatement.fromToken( token.getRequired!(Tok!"<Stmt>")() );
	}

	override @property int dPriority() { return -1; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!stmt));
	}

	override string toDExpr()
	{
		//TODO? Instead of a delegate literal, do "auto tmp = stmt; ...tmp..."
		//      Requires inserting a statement into the current scope just
		//      before the current enclosing statement.
		//      But, could that mess up order-of-execution?
		//      Or does DMD just optimize away the dg already? (No, not yet,
		//      but a Bugzilla ticket and partial patch exists)
		return "(){ "~stmt.toD()~" }()";
	}
}

class HxExprAssign : HxExpression
{
	TokAssign token;
	string op;

	this(TokAssign token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
		op = token.getRequired!(Tok!"<AssignOp>")()[0].toString()[0..$-1];
		lhs = HxExpression.fromToken(token[0], this);
		rhs = HxExpression.fromToken(token[2], this);
	}

	override @property int  dPriority()    { return 4; }
	override @property bool isDLeftAssoc() { return false; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!lhs));
		v.yield(mixin(toNamedNode!rhs));
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.name ~= "("~op~")";
		node.addAttribute("op", op);
	}

	override string toDExpr()
	{
		return lhs.toD()~" "~op~"= "~rhs.toD();
	}
}

class HxExprDot : HxExpression
{
	TokDot token;

	this(TokDot token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
		lhs = HxExpression.fromToken(token[0], this);
		rhs = HxExpression.fromToken(token[2], this);
	}

	override @property int dPriority() { return 17; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!lhs));
		v.yield(mixin(toNamedNode!rhs));
	}

	override string toDExpr()
	{
		return lhs.toD()~"."~rhs.toD();
	}
}

class HxExprCall : HxExpression
{
	TokCall token;
	HxExpression target;
	HxExpression[] args;

	this(TokCall token, HxNode parent)
	{
		this.token = token;
		target = HxExpression.fromToken( token.getRequired!(Tok!"<SuffixExpr>")(), this );
		args = HxExpression.findAll( token.getRequired!(Tok!"<ExprListOpt>")(), this );
		super(parent, target, null);
	}

	override @property int dPriority() { return 17; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!target));

		foreach(i, elem; args)
			v.yield(NamedNode("args[%s]".format(i), elem));
	}

	override string toDExpr()
	{
		return target.toD()~"("~args.toD()~")";
	}
}

class HxExprIndex : HxExpression
{
	TokIndex token;
	HxExpression target;
	HxExpression index;

	this(TokIndex token, HxNode parent)
	{
		this.token = token;
		target = HxExpression.fromToken( token.getRequired!(Tok!"<SuffixExpr>")(), this );
		index = HxExpression.fromToken( token.getRequired!(Tok!"<Expr>")(), this );
		super(parent, target, null);
	}

	override @property int dPriority() { return 17; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!target));
		v.yield(mixin(toNamedNode!index));
	}

	override string toDExpr()
	{
		return target.toD()~"["~index.toD()~"]";
	}
}

class HxExprParen : HxExpression
{
	Tok!"<Value>" token;
	HxExpression expr;

	this(Tok!"<Value>" token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
		if(cast(TokParenExpr)token)
			expr = HxExpression.fromToken(token[1], this);
		else if(auto tokParenStmt = cast(TokParenStmt)token)
			expr = new HxExprStatement(tokParenStmt, this);
		else
			internalError(token, "Unexpected token type passed to HxExprParen constructor");
	}

	override @property int dPriority() { return 17; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!expr));
	}

	override string toDExpr()
	{
		return "("~expr.toD()~")";
	}
}

class HxValueIdent : HxExpression
{
	Tok!"Ident" token;
	string value;

	this(Tok!"Ident" token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
		value = token.toString();
	}

	override @property int dPriority() { return 20; }

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("value", value);
	}

	override string toDExpr()
	{
		return value;
	}
}

class HxValueDecLit : HxExpression
{
	Tok!"DecLit" token;
	string value;

	this(Tok!"DecLit" token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
		value = token.toString();
	}

	override @property int dPriority() { return 20; }

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("value", value);
	}

	override string toDExpr()
	{
		return value;
	}
}

class HxValueStrLit : HxExpression
{
	Tok!"StrLit" token;
	string value;

	this(Tok!"StrLit" token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
		value = token.toString();
		
		if(value[0] == '\'')
		{
			value = value[1..$-1];
			value = value.replace(`"`, `\"`);
			value = `"` ~ value ~ `"`;
		}
	}

	override @property int dPriority() { return 20; }

	override void visit(HxNodeVisitor v)
	{
		// Do nothing
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.addAttribute("value", value);
	}

	override string toDExpr()
	{
		return value;
	}
}

class HxExprUnaryPrefix : HxExpression
{
	Tok!"<PrefixExpr>" token;
	string op;
	HxExpression expr;

	this(Tok!"<PrefixExpr>" token, HxNode parent)
	{
		if( cast(Tok!("<PrefixExpr>", "<NewExpr>")) token )
			internalError(token, "Use HxExprNew for <NewExpr>, not HxExprUnaryPrefix");

		if( cast(Tok!("<PrefixExpr>", "<Cast>")) token )
			internalError(token, "Use HxExprCast for <Cast>, not HxExprUnaryPrefix");

		if( cast(Tok!("<PrefixExpr>", "<SuffixExpr>")) token )
			internalError(token, "Don't bother to create a HxExprUnaryPrefix for <PrefixExpr> ::= <SuffixExpr>");
		
		this.token = token;
		op = token[0].toString();
		expr = HxExpression.fromToken(token[1], this);
		super(parent, null, expr);
	}

	override @property int dPriority() { return 15; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!expr));
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.name ~= "("~op~")";
		node.addAttribute("op", op);
	}

	override string toDExpr()
	{
		return op ~ expr.toD();
	}
}

class HxExprUnarySuffix : HxExpression
{
	Tok!"<SuffixExpr>" token;
	string op;
	HxExpression expr;

	this(Tok!"<SuffixExpr>" token, HxNode parent)
	{
		if( cast(TokDot) token )
			internalError(token, "Use HxExprDot for \"<SuffixExpr> ::= <SuffixExpr> '.' Ident\", not HxExprUnarySuffix");

		if( cast(TokCall) token )
			internalError(token, "Use HxExprCall for \"<SuffixExpr> ::= <SuffixExpr> '(' <ExprListOpt> ')'\", not HxExprUnarySuffix");

		if( cast(TokIndex) token )
			internalError(token, "Use HxExprIndex for \"<SuffixExpr> ::= <SuffixExpr> '[' <Expr> ']'\", not HxExprUnarySuffix");
		
		this.token = token;
		op = token[1].toString();
		expr = HxExpression.fromToken(token[0], this);
		super(parent, expr, null);
	}

	override @property int dPriority() { return 17; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!expr));
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.name ~= "("~op~")";
		node.addAttribute("op", op);
	}

	override string toDExpr()
	{
		return expr.toD() ~ op;
	}
}

class HxExprBinary : HxExpression
{
	Tok!() token;
	string op;
	int haxePriority;
	
	static HxExprBinary[][string] all;
	
	this(Tok!() token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
		
		lhs = HxExpression.fromToken(token[0], this);

		if(cast(Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", ">", ">", "<AddExpr>"))token)
		{
			op  = ">>";
			rhs = HxExpression.fromToken(token[3], this);
		}
		else if(cast(Tok!("<CmpBitShiftExpr>", "<CmpBitShiftExpr>", ">", ">", ">", "<AddExpr>"))token)
		{
			op  = ">>>";
			rhs = HxExpression.fromToken(token[4], this);
		}
		else
		{
			op  = token[1].toString();
			rhs = HxExpression.fromToken(token[2], this);
		}

		switch(op)
		{
		case "||" : haxePriority = 2; _dPriority = 6;  break;
		case "&&" : haxePriority = 3; _dPriority = 7;  break;
		case "...": haxePriority = 4; _dPriority = 1;  break;

		case "==" : haxePriority = 5; _dPriority = 11; break;
		case "!=" : haxePriority = 5; _dPriority = 11; break;
		case ">"  : haxePriority = 5; _dPriority = 11; break;
		case ">=" : haxePriority = 5; _dPriority = 11; break;
		case "<"  : haxePriority = 5; _dPriority = 11; break;
		case "<=" : haxePriority = 5; _dPriority = 11; break;

		case "|"  : haxePriority = 6; _dPriority = 8;  break;
		case "&"  : haxePriority = 6; _dPriority = 10; break;
		case "^"  : haxePriority = 6; _dPriority = 9;  break;
		
		case "<<" : haxePriority = 7; _dPriority = 12; break;
		case ">>" : haxePriority = 7; _dPriority = 12; break;
		case ">>>": haxePriority = 7; _dPriority = 12; break;
		
		case "+"  : haxePriority = 8; _dPriority = 13; break;
		case "-"  : haxePriority = 8; _dPriority = 13; break;
		
		case "*"  : haxePriority = 9; _dPriority = 14; break;
		case "/"  : haxePriority = 9; _dPriority = 14; break;
		
		case "%"  : haxePriority = 10; _dPriority = 14; break;
		
		default:
			internalError(token, "Unhandled operator '"~op~"'");
			break;
		}
		
		all[op] ~= this;
	}

	// Should only be used by prioritizeLHS and prioritizeRHS below
	private this()
	{
		super(null, null, null);
	}
	
	private int _dPriority;
	override @property int dPriority() { return _dPriority; }

	override @property int isAmbiguousDPriority(HxExpression target)
	{
		auto targetExpr = cast(HxExprBinary)target;
		if(!targetExpr)
			return false;
		
		bool isAmbiguous(string op1, string op2)
		{
			if(op1 == "&" || op1 == "^" || op1 == "|")
			if(["==", "!=", ">", ">=", "<", "<="].contains(op2))
				return true;
			
			return false;
		}
		
		return
			isAmbiguous(targetExpr.op, op) ||
			isAmbiguous(op, targetExpr.op);
	}

	void prioritizeLHS()
	{
		auto target = cast(HxExprBinary)lhs;
		if(!target)
			internalError(token, "Can't swap nodes of different types");
		
		// Move LHS to this position
		
		// Swap data (except child/parent references) between 'this' and 'this.lhs'.
		// Ie, make the parent point to the data in 'this.lhs' instead of the data in 'this'.
		auto tmp = new HxExprBinary();
		tmp.copyFrom(target);
		target.copyFrom(this);
		this.copyFrom(tmp);
		
		// Rearrange links
		auto tmp2 = rhs;
		rhs = lhs;
		lhs = target.lhs;
		target.lhs = target.rhs;
		target.rhs = tmp2;
		
		// Fix parent links
		auto _rhs = cast(HxExprBinary)rhs;
		lhs.parent = this;
		_rhs.rhs.parent = rhs;
		assert(rhs.parent is this);
		assert(_rhs.lhs.parent is rhs);
		
		// token has been invalidated
		token = null;
		_rhs.token = null;
	}

	void prioritizeRHS()
	{
		auto target = cast(HxExprBinary)rhs;
		if(!target)
			internalError(token, "Can't swap nodes of different types");
		
		// Swap data (except child/parent references) between 'this' and 'this.rhs'.
		// Ie, make the parent point to the data in 'this.rhs' instead of the data in 'this'.
		auto tmp = new HxExprBinary();
		tmp.copyFrom(target);
		target.copyFrom(this);
		this.copyFrom(tmp);
		
		// Rearrange links
		auto tmp2 = lhs;
		lhs = rhs;
		rhs = target.rhs;
		target.rhs = target.lhs;
		target.lhs = tmp2;
		
		// Fix parent links
		auto _lhs = cast(HxExprBinary)lhs;
		rhs.parent = this;
		_lhs.lhs.parent = lhs;
		assert(lhs.parent is this);
		assert(_lhs.rhs.parent is lhs);
		
		// token has been invalidated
		token = null;
		_lhs.token = null;
	}

	void copyFrom(HxExprBinary from)
	{
		token = from.token;
		op    = from.op;
		haxePriority = from.haxePriority;
		_dPriority   = from._dPriority;
	}

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!lhs));
		v.yield(mixin(toNamedNode!rhs));
	}

	override void customizeTreeNode(TreeNode node)
	{
		node.name ~= "("~op~")";
		node.addAttribute("op", op);
		node.addAttribute("haxePriority", haxePriority);
	}

	override string toDExpr()
	{
		//TODO: Set ... as not yet implemented.
		//if(op == "...")
		//{
			//notImplemented(token, "Operator ...");
		//	return 
		//}
		
		return lhs.toD()~op~rhs.toD();
	}
}

class HxExprNew : HxExpression
{
	Tok!"<NewExpr>" token;
	HxType type;
	HxExpression[] args;

	this(Tok!"<NewExpr>" token, HxNode parent)
	{
		super(parent, null, null);
		this.token = token;
		type = new HxType( token.getRequired!(Tok!"<Type>")() );
		args = HxExpression.findAll( token.getRequired!(Tok!"<ExprListOpt>")(), this );
	}

	override @property int dPriority() { return 15; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!type));

		foreach(i, elem; args)
			v.yield(NamedNode("args[%s]".format(i), elem));
	}

	override string toDExpr()
	{
		return "new "~type.toD()~"("~args.toD()~")";
	}
}

class HxExprCast : HxExpression
{
	TokCast token;
	HxType type;
	HxExpression expr;

	this(TokCast token, HxNode parent)
	{
		notImplemented(token, "Proper semantics for type casting");

		super(parent, null, null);
		this.token = token;
		type = new HxType( token.getRequired!(Tok!"<FuncType>")() );
		expr = HxExpression.fromToken( token.getRequired!(Tok!"<Expr>")(), this );
	}

	override @property int dPriority() { return 15; }

	override void visit(HxNodeVisitor v)
	{
		v.yield(mixin(toNamedNode!type));
		v.yield(mixin(toNamedNode!expr));
	}

	override string toDExpr()
	{
		return "cast("~type.toD()~")"~expr.toD();
	}
}
