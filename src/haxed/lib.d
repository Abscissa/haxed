// HaxeD: Compile Haxe code to D
// Written in the D programming language.

/++
The goal of this is to compile Haxe code to D.

It is recommended that you use this module as a renamed import:
--
import HaxeD = haxed.lib;
HaxeD.compile(...);
--

$(WEB http://www.dsource.org/projects/haxed/, HaxeD Homepage)

Author:
$(WEB www.semitwist.com, Nick Sabalausky)

This should work with DMD 2.058 and up

This requires SemiTwist D Tools and Goldie trunk. See HaxeD Homepage link
above for instructions.
+/

module haxed.lib;

import std.conv;
import std.file;
import std.stdio;
import std.string;

import semitwist.util.all;
import haxed.ast;
import haxed.util;

alias haxed.util.CompileException CompileException;
alias haxed.util.InternalHaxeDException InternalHaxeDException;
alias haxed.util.Options Options;
alias haxed.util.Data Data;

enum headerInfo = ("
	HaxeD v"~haxedVerStr~" - Compile Haxe code to D
	Copyright (c) 2011-2012 Nick Sabalausky
	See LICENSE.txt for license info
	Site: http://www.dsource.org/projects/haxed/
").normalize()~"\n";

enum usageInfo = ("
	Usage: haxed [options] [.hx files] [options]
").normalize()~"\n";

enum optionsInfo = ("
	--help, /?                  Show this help screen
	--cp, --import-path path    Add import search path
	--D,  --define ident        Define preprocessor identifier
	--dout path                 Generate D files into target path (default: .)
	--ignore-imports            Don't automatically load and process imports
	--r,  --rdmd-arg arg        (unused ATM) Pass argument to RDMD
	--save-prep                 Save preprocesser output
	--ast                       Save the Abstract Syntax Trees
	--translate-only            (unused ATM) Just transate to D, don't run RDMD
").normalize()~"\n";

enum helpScreen = headerInfo ~ "\n" ~ usageInfo ~ "\n" ~ optionsInfo;

Data compile(string[] files, Options options)
{
	data = Data();
	data.opt = options;

	// Predefine 'dlang'
	if(!data.opt.defines.contains("dlang"))
		data.opt.defines ~= "dlang";
	
	// Fixup paths
	data.opt.importPaths ~= "./";
	foreach(ref path; data.opt.importPaths)
	if(path.endsWith("/", "\\") == 0)
		path ~= "/";

	if(data.opt.dout.endsWith("/", "\\") == 0)
		data.opt.dout ~= "/";

	foreach(filepath; files)
		loadHx(filepath);
	
	fixOrderOfOperations();
	
	if(data.hasNotImplemented)
		showNotImplementedHelp();

	if(data.missingImports.length > 0)
	{
		writeln("Searched for modules in these import paths:");
		foreach(path; data.opt.importPaths)
			writeln("  ", path);
		
		throw new CompileException(data, null);
	}
	
	if(data.hasError)
		throw new CompileException(data, null);
	
	if(data.opt.saveAST)
	foreach(mod; data.modules)
		mod.saveAST();
	
	saveDFiles();

	if(data.hasNotImplemented)
		showNotImplementedHelp();

	if(data.hasError)
		throw new CompileException(data, null);
	
	return data;
}

private void fixOrderOfOperations()
{
	bool hasChanged;
	do
	{
		hasChanged = false;

		foreach(op; ["<<", ">>", ">>>", "|", "&", "^", "==", "!=", ">", ">=", "<", "<="])
		if(op in HxExprBinary.all)
		foreach(_expr; HxExprBinary.all[op])
		{
			auto expr = cast(HxExprBinary)_expr;
			auto lhs  = cast(HxExprBinary)expr.lhs;
			auto rhs  = cast(HxExprBinary)expr.rhs;
			
			if(lhs && lhs.haxePriority < expr.haxePriority)
			{
				expr.prioritizeLHS();
				hasChanged = true;
			}

			if(rhs && rhs.haxePriority <= expr.haxePriority)
			{
				expr.prioritizeRHS();
				hasChanged = true;
			}
		}

	} while(hasChanged);
}

private void saveDFiles()
{
	foreach(mod; data.modules)
		mod.saveD();
}
