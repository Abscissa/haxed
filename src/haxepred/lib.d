// HaxePreD: Haxe Preprocessor In D
// Written in the D programming language

/++
A preprocessor for the Haxe language (http://www.haxe.org).

Author:
$(WEB www.semitwist.com, Nick Sabalausky)

This should work with DMD 2.058 and up

This requires SemiTwist D Tools and Goldie trunk. See HaxeD Homepage link
above for instructions.
+/

module haxepred.lib;

import std.stdio;
import std.string;
import std.uni;

import semitwist.util.all;
import goldie.all;
import lang.expr.all;
import haxepred.util;

private alias language_expr exprLang;

class PreprocessException : Exception
{
	uint line;
	uint col;
	long index;
	long[] lineIndicies;
	
	this(HaxePreD hpd, string msg)
	{
		this.line = hpd.line;
		this.index = hpd.index;
		this.lineIndicies = hpd.lineIndicies;
		this.col = cast(uint)(index - lineIndicies[HaxePreD.lineAtIndex(lineIndicies, index)]);
		super("(%s:%s): %s".format(line+1, col+1, msg));
	}
}

class ErrorDirectiveException : PreprocessException
{
	this(HaxePreD hpd)
	{
		super(hpd, "Not implemented for current platform");
	}
}

class InvalidDirectiveException : PreprocessException
{
	this(HaxePreD hpd, string directive)
	{
		super(hpd, "Invalid preprocessor directive '"~directive~"'");
	}
}

class NoMatchingIfException : PreprocessException
{
	this(HaxePreD hpd, string directive, bool directlyInElse=false)
	{
		super(hpd,
			"'%s' without matching '#if'%s"
				.format(
					directive,
					(directlyInElse? ", directly inside an '#else'":"")
				)
		);
	}
}

class UnterminatedIfException : PreprocessException
{
	this(HaxePreD hpd)
	{
		super(hpd, "Unterminated '#if'");
	}
}

class UnexpectedEOFException : PreprocessException
{
	this(HaxePreD hpd, string state)
	{
		super(hpd, "Unexpected EOF inside '%s'".format(state));
	}
}

class ConditionalExpressionException : PreprocessException
{
	Exception next;
	string expr;
	
	this(HaxePreD hpd, string expr, Exception next)
	{
		this.next = next;
		this.expr = expr;
		super(hpd, "Invalid conditional expression '%s': %s".format(expr, next));
	}
}

class InternalPreprocessException : PreprocessException
{
	this(HaxePreD hpd, string msg)
	{
		super(hpd, "Internal Error: "~msg);
	}
}

//TODO: Change err msg from "characters {end index}-{end index}" to "characters {start column}-{end column}"
//TODO: Report the location where errors start, rather than where they are detected.

class HaxePreD
{
	/// input:   Haxe source code.
	/// defines: A list of identifiers to be considered to be defined.
	/// Returns the preprocessed result.
	public static string preprocessFile(string filename, string[] defines=[])
	{
		return preprocessString(readUTFFile!string(filename), defines);
	}
	
	/// input:   Haxe source code.
	/// defines: A list of identifiers to be considered to be defined.
	/// Returns the preprocessed result.
	public static string preprocessString(string input, string[] defines=[])
	{
		auto hpd = new HaxePreD();

		hpd.inBuf = new ReadAdjacentBuffer(input);
		hpd.defines = defines;
		return hpd.process(false);
	}
	
	/// The indicies on which each line starts.
	/// Ex: Line #4 (zero-indexed) starts at index 'getLastLineIndicies()[4]'
	public static long[] getLastLineIndicies()
	{
		return lastLineIndicies;
	}
	
	private static long[] lastLineIndicies;
	
	public static ptrdiff_t lineAtIndex(long[] lineIndicies, long index)
	{
		foreach_reverse(line, lineStart; lineIndicies)
		{
			if(lineStart < index)
				return line;
		}
		return 0;
	}
	
	private static Lexer_expr  goldLexer;
	private static Parser_expr goldParser;
	
	private ReadAdjacentBuffer inBuf;
	
	private this()
	{
		if(!goldLexer ) goldLexer  = new Lexer_expr();
		if(!goldParser) goldParser = new Parser_expr();
	}

	private Token parseCond(string condStr)
	{
		goldParser.process(goldLexer.process(condStr, exprLang), exprLang);
		return goldParser.parseTreeX;
	}

	private bool evalCond(string condStr)
	{
		Token pt;
		try
			pt = parseCond(condStr);
		catch(Exception e)
			throw new ConditionalExpressionException(this, condStr, e);

		return evalCond(pt);
	}
	
	private bool evalCond(Token tok)
	{
		//mixin(traceVal!("tok.name", "tok"));
		switch(tok.name)
		{
		case "Ident":
			return cast(bool)(defines.contains(tok.toString()));
		
		case "<ValExpr>":
			// Ident
			if(tok.subX.length == 1)
				return evalCond(tok.subX[0]);
				
			// '!' Ident
			if(tok.subX.length == 2)
				return !evalCond(tok.subX[1]);
			
			// '(' <OrExpr> ')'
			if(tok.subX.length == 3)
				return evalCond(tok.subX[1]);

			// '!' '(' <OrExpr> ')'
			return !evalCond(tok.subX[2]);
		
		case "<OrExpr>":
			// <AndExpr>
			if(tok.subX.length == 1)
				return evalCond(tok.subX[0]);
			
			// <OrExpr> '||' <AndExpr>
			return evalCond(tok.subX[0]) || evalCond(tok.subX[2]);
			
		case "<AndExpr>":
			// <ValExpr>
			if(tok.subX.length == 1)
				return evalCond(tok.subX[0]);
			
			// <AndExpr> '&&' <ValExpr>
			return evalCond(tok.subX[0]) && evalCond(tok.subX[2]);
			
		default:
			throw new InternalPreprocessException(this,
				"Unexpected token type '%s' in conditional expression".format(tok.name)
			);
		}
	}
	
	private enum State
	{
		Normal,
		Keyword,
		LineComment,
		BlockComment,
		SingleQuote,
		DoubleQuote,
		Regex,
		CondExpr,
		CondExpr_Ident,
		CondExpr_Paren,
	}
	
	private static string stateToReadableString(State state)
	{
		string[State] lookup =
		[
			State.Normal:         "Haxe Source",
			State.Keyword:        "Preprocessor Directive",
			State.LineComment:    "Line Comment",
			State.BlockComment:   "Block Comment",
			State.SingleQuote:    "Single Quote",
			State.DoubleQuote:    "Double Quote",
			State.Regex:          "Regular Expression",
			State.CondExpr:       "Conditional Expression",
			State.CondExpr_Ident: "Conditional Expression",
			State.CondExpr_Paren: "Conditional Expression",
		];
		
		return (state in lookup)? lookup[state] : "{unknown: #%s}".format(state);
	}

	private string[] defines;
	private State state;
	private long index;
	private uint line;
	private long[] lineIndicies;
	private string tokenStr;
	private bool eof;
	private char ch;
	private int parenDepth;

	private bool streamOut;
	private string outStr;
	
	private int charsWritten;
	
	private static enum allDirectives = ["#if", "#else", "#elseif", "#end", "#error"];

	private struct IfState
	{
		bool isTrue;
		bool isInElse;
		bool isInElseIf;
	}
	private IfState[] ifStateStack;
	
	private bool isTrue()
	{
		foreach(IfState state; ifStateStack)
			if(!state.isTrue)
				return false;
		
		return true;
	}
	
	private void reset()
	{
		state = State.Normal;
		index = 0;
		line = 0;
		lineIndicies = [0];
		eof = false;
		outStr = "";
		ifStateStack = [];
		charsWritten = 0;
		tokenStr = "";
		parenDepth = 0;
	}
	
	// Sends a char to output. Replaces disabled code with spaces.
	private void output(char outChar = ' ')
	{
		debug charsWritten++;
		outStr ~= (isTrue() || isWhite(ch))? outChar : ' ';
	}

	private void onDirective(string tok)
	{
		switch(tok)
		{
		case "#if":
			state = State.CondExpr;
			tokenStr = "";
			break;
			
		case "#else":
			if(ifStateStack.length == 0)
				throw new NoMatchingIfException(this, tok);
			if(ifStateStack[$-1].isInElse)
				throw new NoMatchingIfException(this, tok, true);
			ifStateStack[$-1].isInElse = true;
			ifStateStack[$-1].isTrue = !ifStateStack[$-1].isTrue;
			state = State.Normal;
			break;
			
		case "#elseif":
			if(ifStateStack.length == 0)
				throw new NoMatchingIfException(this, tok);
			if(ifStateStack[$-1].isInElse)
				throw new NoMatchingIfException(this, tok, true);
			ifStateStack[$-1].isInElseIf = true;
			state = State.CondExpr;
			tokenStr = "";
			break;
			
		case "#end":
			if(ifStateStack.length == 0)
				throw new NoMatchingIfException(this, tok);
			ifStateStack = ifStateStack[0..$-1]; // Pop
			state = State.Normal;
			break;
			
		case "#error":
			if(ifStateStack.length == 0 || ifStateStack[$-1].isTrue)
				throw new ErrorDirectiveException(this);
			break;
			
		default:
			throw new InternalPreprocessException(this, "Unexpected directive '%s'".format(tok));
		}
	}
	
	private bool isLetterOrDigit(char ch)
	{
		return
			(ch >= 'a' && ch <= 'z') ||
			(ch >= 'A' && ch <= 'Z') ||
			(ch >= '0' && ch <= '9');
	}
	
	private string process(bool streamOut)
	{
		reset();
		this.streamOut = streamOut;
		while(!inBuf.eof)
		{
			ch = inBuf.curr;
			if(ch == '\n')
			{
				line++;
				lineIndicies ~= index;
			}
			
			switch(state)
			{
			case State.Normal:
				switch(ch)
				{
				case '#':
					state = State.Keyword;
					tokenStr = ch~"";
					output(' ');
					break;
					
				case '/':
					if(inBuf.prev.length > 0)
					{
						if(inBuf.prev[$-1] == '/')
							state = State.LineComment;
						else if(inBuf.prev[$-1] == '~')
							state = State.Regex;
					}

					output(ch);
					break;
					
				case '*':
					if(inBuf.prev.length > 0 && inBuf.prev[$-1] == '/')
							state = State.BlockComment;

					output(ch);
					break;
					
				case '\'':
					state = State.SingleQuote;
					output(ch);
					break;

				case '"':
					state = State.DoubleQuote;
					output(ch);
					break;
				
				default:
					output(ch);
					break;
				}
				break;
			
			case State.Keyword:
				output(' ');
				tokenStr ~= ch;

				if(inBuf.next.length == 0 || !isLower(inBuf.next[0]))
				{
					if(allDirectives.contains(tokenStr))
						onDirective(tokenStr);
					else
						throw new InvalidDirectiveException(this, tokenStr);
				}
				break;

			case State.LineComment:
				output(ch);

				if(ch == '\n')
					state = State.Normal;
				break;

			case State.BlockComment:
				output(ch);

				if(ch == '/' && inBuf.prev.length > 0 && inBuf.prev[$-1] == '*')
					state = State.Normal;
				break;

			case State.SingleQuote:
				output(ch);

				if(ch == '\'' && (inBuf.prev.length == 0 || inBuf.prev[$-1] != '\\'))
					state = State.Normal;
				break;

			case State.DoubleQuote:
				output(ch);

				if(ch == '"' && (inBuf.prev.length == 0 || inBuf.prev[$-1] != '\\'))
					state = State.Normal;
				break;

			case State.Regex:
				output(ch);

				// Don't need to worry about the "igms" suffixes,
				// because State.Normal will handle them just fine anyway.
				if(ch == '/' && (inBuf.prev.length == 0 || inBuf.prev[$-1] != '\\'))
					state = State.Normal;
				break;

			case State.CondExpr:
				tokenStr ~= ch;
				
				if(isWhite(ch))
					output(ch);
				else
				{
					output(' ');
					
					if(ch != '!')
					{
						if(ch == '(')
						{
							state = State.CondExpr_Paren;
							parenDepth = 1;
						}
						else
							state = State.CondExpr_Ident;
					}
				}
				break;
				
			case State.CondExpr_Ident:
				tokenStr ~= ch;

				bool identDone=false;
				if(isWhite(ch))
				{
					output(ch);
					identDone = true;
				}
				else
				{
					output(' ');
					if(!isLetterOrDigit(ch) && ch != '_')
						identDone = true;
				}
					
				if(identDone)
				{
					bool cond = evalCond(tokenStr);
					if(ifStateStack.length > 0 && ifStateStack[$-1].isInElseIf)
					{
						if(ifStateStack[$-1].isTrue)
							ifStateStack[$-1].isTrue = false;
						else
							ifStateStack[$-1].isTrue = cond;
					}
					else
						ifStateStack ~= IfState(cond, false, false); // Push
					
					state = State.Normal;
				}
				break;

			case State.CondExpr_Paren:
				tokenStr ~= ch;

				if(isWhite(ch))
					output(ch);
				else
				{
					output(' ');
					if(ch == '(')
						parenDepth++;
					else if(ch == ')')
					{
						parenDepth--;
						if(parenDepth == 0)
						{
							ifStateStack ~= IfState(evalCond(tokenStr), false); // Push
							state = State.Normal;
						}
					}
				}
				break;

			default:
				throw new InternalPreprocessException(this, "Unknown state '%s'".format(state));
			}
			
			debug
			{
				if(charsWritten != 1)
					throw new InternalPreprocessException(this, "Wrong number of bytes generated (expected 1, got %s)".format(charsWritten));
				charsWritten = 0;
			}
			
			index++;
			inBuf.advance();
		}
		
		if(state != State.Normal)
			throw new UnexpectedEOFException(this, stateToReadableString(state));
		
		if(ifStateStack.length != 0)
			throw new UnterminatedIfException(this);
			
		return outStr;
	}
}

