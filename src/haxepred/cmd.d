// HaxePreD: Haxe Preprocessor In D
// Written in the D programming language

module haxepred.cmd;

import std.stdio;

import semitwist.cmdlineparser;
import semitwist.util.all;

//TODO? If neither /o nor /so is specified, automatically assume "/o:infile{.preproc.hx}"

class CmdSwitches
{
	bool shouldExit;

	public this(string[] args)
	{
		init();
		shouldExit = !parse(args);
	}
	
	// Direct command-line args
	bool help;
	bool morehelp;

	bool useStdin;
	bool useStdout;

	string inFilename;
	string outFilename;

	string[] defines;

	// Private ------
	private CmdLineParser cmd;
	private void init()
	{
		cmd = new CmdLineParser();
		mixin(defineArg!(cmd, "help",     help,        ArgFlag.Optional, "Displays a help summary and exits" ));
		mixin(defineArg!(cmd, "morehelp", morehelp,    ArgFlag.Optional, "Displays a detailed help message and exits" ));
		mixin(defineArg!(cmd, "",         inFilename,  ArgFlag.Required, "Input filename" ));
		mixin(defineArg!(cmd, "o",        outFilename, ArgFlag.Required, "Output filename" ));
		mixin(defineArg!(cmd, "d",        defines,     ArgFlag.Optional, "Define identifier" ));
	}
	
	enum sampleUsageMsg = 
"Sample Usage:
    haxepred myfile.hx -o:preprocessed.hx -d:php -d:DEBUG
";

	// Returns: Should processing proceed? If false, the program should exit.
	private bool parse(string[] args)
	{
		cmd.parse(args);
		if(morehelp)
		{
			writeln(sampleUsageMsg);
			write(cmd.getDetailedUsage());
			return false;
		}
		
		if(!cmd.success || help)
		{
			writeln(sampleUsageMsg);
			write(cmd.getUsage(16));
			if(!help)
			{
				writeln();
				write(cmd.errorMsg);
			}
			return false;
		}

		return true;
	}
	
}
