// HaxePreD: Haxe Preprocessor In D
// Written in the D programming language

/++
A preprocessor for the Haxe language (http://www.haxe.org).

Author:
$(WEB www.semitwist.com, Nick Sabalausky)

This should work with DMD 2.058 and up

This requires SemiTwist D Tools and Goldie master. See HaxeD Homepage link
above for instructions.
+/

module haxepred.main;

import std.stdio;
import std.string;

import semitwist.util.all;

import haxepred.cmd;
import haxepred.lib;

int main(string[] args)
{
	flushAsserts();
	
	auto cmdsw = new CmdSwitches(args);
	if(cmdsw.shouldExit)
		return 1;
	
	try
	{
		auto outputData = HaxePreD.preprocessFile(cmdsw.inFilename, cmdsw.defines);
		std.file.write(cmdsw.outFilename, outputData);
	}
	catch(PreprocessException e)
	{
		writefln("%s%s", cmdsw.inFilename, e.msg);
		return 1;
	}
	
	return 0;
}

unittest
{
	// BE VERY CAREFUL NOT TO CHANGE ANY WHITESPACE INSIDE THESE STRINGS!
	// (Line endings can be changed though, as long as the change is
	// consistent throughout.)
	enum src1 =
"#if AB
	AB
	#if XY
		AB && XY
	#else
		AB && !XY
	#end
	AB
#else
	!AB
	#if XY
		!AB && XY
	#else
		!AB && !XY
	#end
	!AB
#end";
	enum res1 =
"      
	  
	      
		        
	     
		         
	    
	  
     
	!AB
	      
		         
	     
		!AB && !XY
	    
	!AB
    ";
	enum res1_AB =
"      
	AB
	      
		        
	     
		AB && !XY
	    
	AB
     
	   
	      
		         
	     
		          
	    
	   
    ";
	enum res1_XY =
"      
	  
	      
		        
	     
		         
	    
	  
     
	!AB
	      
		!AB && XY
	     
		          
	    
	!AB
    ";
	enum res1_AB_XY =
"      
	AB
	      
		AB && XY
	     
		         
	    
	AB
     
	   
	      
		         
	     
		          
	    
	   
    ";

	mixin(deferAssert!(`HaxePreD.preprocessString(src1) == res1`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src1, ["AB"]) == res1_AB`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src1, ["XY"]) == res1_XY`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src1, ["AB", "XY"]) == res1_AB_XY`));

	enum src2 =
"hi//#error
#if DEFTHIS
stuff
#else
foobar
#end
hey/*#error*/whoo
#if DEFTHIS
stuff
#else
foobar
#end";
	enum res2 =
"hi//#error
           
stuff
     
      
    
hey/*#error*/whoo
           
stuff
     
      
    ";
	mixin(deferAssert!(`HaxePreD.preprocessString(src2, ["DEFTHIS"]) == res2`));

	enum src3 =
"/*#error*/#if DEFTHIS
hi
#end";
	enum res3 =
"/*#error*/           
hi
    ";
//	mixin(deferAssert!(`HaxePreD.preprocessString(src3, ["DEFTHIS"]) == res3`));
//mixin(traceVal!(`HaxePreD.preprocessString(src3, ["DEFTHIS"])`));

	enum src4 =
`hi"#error"!
#if DEFTHIS
stuff
#else
foobar
#end
hey'#error'whoo
#if DEFTHIS
stuff
#else
foobar
#end`;
	enum res4 =
`hi"#error"!
           
stuff
     
      
    
hey'#error'whoo
           
stuff
     
      
    `;
	mixin(deferAssert!(`HaxePreD.preprocessString(src4, ["DEFTHIS"]) == res4`));
	
	enum src5 =
`"#error"#if DEFTHIS
hi
#end`;
	enum res5 =
`"#error"           
hi
    `;
	mixin(deferAssert!(`HaxePreD.preprocessString(src5, ["DEFTHIS"]) == res5`));

	enum src6 =
`'#error'#if DEFTHIS
hi
#end`;
	enum res6 =
`'#error'           
hi
    `;
	mixin(deferAssert!(`HaxePreD.preprocessString(src6, ["DEFTHIS"]) == res6`));

	enum src7 =
`~/#error/#if DEFTHIS
hi
#end`;
	enum res7 =
`~/#error/           
hi
    `;
	mixin(deferAssert!(`HaxePreD.preprocessString(src7, ["DEFTHIS"]) == res7`));

	enum src8 = `"#error\"#error"hi`;
	mixin(deferAssert!(`HaxePreD.preprocessString(src8) == src8`));

	enum src9 = `'#error\'#error'`;
	mixin(deferAssert!(`HaxePreD.preprocessString(src9) == src9`));

	enum src10 = `"hello"#error`;
	mixin(deferEnsureThrows!(`HaxePreD.preprocessString(src10);`, ErrorDirectiveException));

	mixin(deferEnsureThrows!(`HaxePreD.preprocessString("/*hello*/#error");`, ErrorDirectiveException));

	enum src11 = `#if AB hi #end`;
	mixin(deferAssert!(`HaxePreD.preprocessString(src11, ["AB"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src11, ["nAB"]).strip() == ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src11, []).strip() == ""`));

	enum src12 = `#if (AB) hi #end`;
	mixin(deferAssert!(`HaxePreD.preprocessString(src12, ["AB"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src12, ["nAB"]).strip() == ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src12, []).strip() == ""`));

	enum src13 = `#if AB #else hi #end`;
	mixin(deferAssert!(`HaxePreD.preprocessString(src13, ["AB"]).strip() == ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src13, ["nAB"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src13, []).strip() == "hi"`));

	enum src14 = `#if !AB hi #end`;
	mixin(deferAssert!(`HaxePreD.preprocessString(src14, ["AB"]).strip() == ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src14, ["nAB"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src14, []).strip() == "hi"`));

	enum src15 = `#if (!AB && ! CD || !WX && YZ ) hi #end`;
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, ["nAB", "nCD", "nWX", "nYZ"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, ["nAB", "nCD", "nWX",  "YZ"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, ["nAB", "nCD",  "WX", "nYZ"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, ["nAB", "nCD",  "WX",  "YZ"]).strip() == "hi"`));

	mixin(deferAssert!(`HaxePreD.preprocessString(src15, ["nAB",  "CD", "nWX", "nYZ"]).strip() ==   ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, ["nAB",  "CD", "nWX",  "YZ"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, ["nAB",  "CD",  "WX", "nYZ"]).strip() ==   ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, ["nAB",  "CD",  "WX",  "YZ"]).strip() ==   ""`));

	mixin(deferAssert!(`HaxePreD.preprocessString(src15, [ "AB", "nCD", "nWX", "nYZ"]).strip() ==   ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, [ "AB", "nCD", "nWX",  "YZ"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, [ "AB", "nCD",  "WX", "nYZ"]).strip() ==   ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, [ "AB", "nCD",  "WX",  "YZ"]).strip() ==   ""`));

	mixin(deferAssert!(`HaxePreD.preprocessString(src15, [ "AB",  "CD", "nWX", "nYZ"]).strip() ==   ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, [ "AB",  "CD", "nWX",  "YZ"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, [ "AB",  "CD",  "WX", "nYZ"]).strip() ==   ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src15, [ "AB",  "CD",  "WX",  "YZ"]).strip() ==   ""`));

	enum src16 = `#if (!(AB || CD && !(!WX))) hi #end`;
	mixin(deferAssert!(`HaxePreD.preprocessString(src16, ["nAB", "nCD", "nWX"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src16, ["nAB", "nCD",  "WX"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src16, ["nAB",  "CD", "nWX"]).strip() == "hi"`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src16, ["nAB",  "CD",  "WX"]).strip() ==   ""`));

	mixin(deferAssert!(`HaxePreD.preprocessString(src16, [ "AB", "nCD", "nWX"]).strip() ==   ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src16, [ "AB", "nCD",  "WX"]).strip() ==   ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src16, [ "AB",  "CD", "nWX"]).strip() ==   ""`));
	mixin(deferAssert!(`HaxePreD.preprocessString(src16, [ "AB",  "CD",  "WX"]).strip() ==   ""`));

	mixin(deferEnsureThrows!(`HaxePreD.preprocessString("#els");`, InvalidDirectiveException));
	mixin(deferEnsureThrows!(`HaxePreD.preprocessString("/*hello");`, UnexpectedEOFException));
	mixin(deferEnsureThrows!(`HaxePreD.preprocessString("//hello");`, UnexpectedEOFException));
	mixin(deferEnsureThrows!(`HaxePreD.preprocessString("#if (hi");`, UnexpectedEOFException));
	mixin(deferEnsureThrows!(`HaxePreD.preprocessString("\"hi");`, UnexpectedEOFException));
	mixin(deferEnsureThrows!(`HaxePreD.preprocessString("'hi");`, UnexpectedEOFException));
	mixin(deferEnsureThrows!(`HaxePreD.preprocessString("~/hi");`, UnexpectedEOFException));
	
	enum src17 =
`1
2
3
4
5
6234567890#error
7
8`;
	mixin(deferEnsureThrows!(`HaxePreD.preprocessString(src17);`, ErrorDirectiveException));
	try
		HaxePreD.preprocessString(src17);
	catch(ErrorDirectiveException e)
	{
		mixin(deferEnsure!(`e.line`, `_==5`));
		mixin(deferEnsure!(`e.col`, `_==16`));
	}
	catch(Throwable e)
		writefln("Caught wrong exception");

}
