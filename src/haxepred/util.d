// HaxePreD: Haxe Preprocessor In D
// Written in the D programming language

module haxepred.util;

// Could I make this faster for streams by treating nextBuf and prevBuf as circular buffers?
// Probably just when next/prev aren't used.
class ReadAdjacentBuffer
{
	private string inBuf;
	private string nextBuf;
	private string prevBuf;
	private char currCh;
	private bool streamEof;
	private int maxPrev;
	
	private string inString;
	private int index;
	
	this(string inString)
	{
		reset(inString);
	}
	
	void reset(string inString)
	{
		reset();
		this.inString = inString;
	}
	
	private void reset()
	{
		inBuf = "";
		nextBuf = "";
		prevBuf = "";
		maxPrev = 0;
		streamEof = false;
		
		inString = "";
		index = 0;
		
		_eof = false;
	}
	
	@property char curr()
	{
		return inString[index];
	}
	
	bool _eof;
	@property bool eof()
	{
		return _eof;
	}
	
	// False if EOF
	bool advance()
	{
		if(!eof)
		{
			index++;
			if(index == inString.length)
				_eof = true;
		}

		return _eof;
	}
	
	string next()
	{
		return inString[index+1..$];
	}

	string prev()
	{
		return inString[0..index];
	}
}

unittest
{
	auto b = new ReadAdjacentBuffer("0123456789");
	
	assert(!b.eof);
	assert(b.curr == '0');
	assert(b.next == "123456789");
	assert(b.prev == "");

	b.advance();
	b.advance();

	assert(!b.eof);
	assert(b.curr == '2');
	assert(b.next == "3456789");
	assert(b.prev == "01");

	b.advance();
	b.advance();
	b.advance();
	b.advance();

	assert(!b.eof);
	assert(b.curr == '6');
	assert(b.next == "789");
	assert(b.prev == "012345");

	b.advance();
	b.advance();
	b.advance();
	assert(!b.eof);
	b.advance();
	assert(b.eof);
	
	b = new ReadAdjacentBuffer("012");
	assert(!b.eof);
	assert(b.curr == '0');
	assert(b.next == "12");
	assert(b.prev == "");
	b.advance();
	assert(!b.eof);
	assert(b.curr == '1');
	assert(b.prev == "0");
	
	/+b = new ReadAdjacentBuffer(new Array("0123456789"), 3, 3);
	assert(!b.eof);
	assert(b.curr == '0');
	assert(b.next == "123");
	assert(b.prev == "");
	
	b.advance();
	b.advance();
	assert(!b.eof);
	assert(b.curr == '2');
	assert(b.next == "345");
	assert(b.prev == "01");
	
	b.advance();
	b.advance();
	b.advance();
	assert(!b.eof);
	assert(b.curr == '5');
	assert(b.next == "678");
	assert(b.prev == "234");

	b.advance();
	b.advance();
	assert(!b.eof);
	assert(b.curr == '7');
	assert(b.next == "89");
	assert(b.prev == "456");

	b.advance();
	b.advance();
	assert(!b.eof);
	b.advance();
	assert(b.eof);+/
	
	/+b = new ReadAdjacentBuffer(new Array("012"), 5, 5);
	assert(!b.eof);
	assert(b.curr == '0');
	assert(b.next == "12");
	assert(b.prev == "");
	
	b.advance();
	b.advance();
	assert(!b.eof);
	b.advance();
	assert(b.eof);
	assert(b.curr == '2');
	assert(b.next == "");
	assert(b.prev == "01");+/
}
