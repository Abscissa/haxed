@echo off

goldie-grmc lang\expr.grm -o=lang\expr.cgt
goldie-grmc lang\hx-alt.grm -o=lang\hx-alt.cgt
echo.
echo The grammar 'lang\hx-alt.grm' is known to have one
echo shift-reduce warning, but it is known to be *OK*.
echo.
echo Generating static-style languages...
goldie-staticlang lang\expr.cgt --dir=src --pack=lang.expr
goldie-staticlang lang\hx-alt.cgt --dir=src --pack=lang.hx
